# SIMPLE CRUD APPLICATION USING LARAVEL
This projects is crud application which handle data products for admin and there are pages inside the web app such as:

- Login.
- Register.
- Home for display list products.
- Add for add products.
- Edit for edit products.

## Installation

1. **You can clone the repository**:
```bash
git clone repository -->link repository<--
```
2. **Make sure you should update the package to able run these project**:
```bash
composer install 
composer update
```
3. **Rename .env.example to .env**:

4. **Create database name in your mysql**:

5. **Make sure the .env variable same according your computer especially database account**:

6. **Run the below to migrating database without create manually**:
```bash
php artisan migrate
```
7. **Run the bellow command to open public storage because the images put there and the display data product read by there**:
```bash
php artisan storage:link
```
8. **You can run the app with execute this command**:
```bash
php artisan serve
```
## Tools
The tools that used for support develop this project such as:

- Apache2.
- PHP 8.2.12 version.
- MYSQL.
- Composer to run this project.
