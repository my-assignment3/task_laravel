<?php

namespace App\Http\Controllers;

use App\Models\UsersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login_page()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'username' => 'required',
                'password' => 'required',
            ]);

            if (!Auth::attempt($validatedData)) {
                throw new \Exception('Username or password is wrong');
            }

            $request->session()->put('isLogin', true);
            $request->session()->put('username', $validatedData['username']);

            return redirect()->route('home_page');
        } catch (\Exception $e) {
            return back()->withErrors([
                'result' => $e->getMessage(),
            ]);
        }
    }

    public function register_page()
    {
        return view('register');
    }

    public function register(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'username' => 'required',
                'email' => 'required|email',
                'password' => 'required|min:5',
                'repeat_password' => 'required|min:5',
            ]);

            if($validatedData['password'] != $validatedData['repeat_password']) {
                throw new \Exception('Password and repeat password should be same');
            }

            $validatedData['password'] = Hash::make($validatedData['password']);

            $user = UsersModel::create($validatedData);

            if (!$user) {
                throw new \Exception('Failed to create user');
            }

            return redirect()->route('login_page');
        } catch (\Exception $e) {
            return back()->withErrors([
                'result' => $e->getMessage(),
            ]);
        }
    }

    public function logout(Request $request)
    {
        $request->session()->forget('isLogin');
        $request->session()->forget('username');
        Auth::logout();

        return redirect()->route('login_page');
    }
}
