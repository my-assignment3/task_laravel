<?php

namespace App\Http\Controllers;

use App\Models\ProductsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class ProductsController extends Controller
{
    public function home_page()
    {
        try {
            $products = ProductsModel::all();
            return view('home', ['products' => $products]);
        } catch (\Exception $e) {
            return back()->withErrors(['error' => 'Error fetching products: ' . $e->getMessage()]);
        }
    }

    public function add_page()
    {
        return view('add');
    }

    public function edit_page($id)
    {
        try {
            $product = ProductsModel::findOrFail($id);
            return view('edit', ['product' => $product]);
        } catch (\Exception $e) {
            return back()->withErrors(['error' => 'Error fetching product details: ' . $e->getMessage()]);
        }
    }

    public function store(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'name' => 'required',
                'price' => 'required|numeric|min:0',
                'description' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            ]);

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time() . '_' . $image->getClientOriginalName();
                $image->storeAs('public/images', $imageName);
            } else {
                throw new \Exception('Image upload failed: No file found.');
            }

            $product = new ProductsModel();
            $product->name = $validatedData['name'];
            $product->price = $validatedData['price'];
            $product->description = $validatedData['description'];
            $product->image = $imageName;
            $product->save();

            return redirect()->route('home_page')->with('success', 'Product added successfully.');
        } catch (\Exception $e) {
            return back()->withErrors(['error' => 'Failed to add product: ' . $e->getMessage()]);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $validatedData = $request->validate([
                'name' => 'required',
                'price' => 'required|numeric|min:0',
                'description' => 'required',
                'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            ]);

            $product = ProductsModel::findOrFail($id);

            $product->name = $validatedData['name'];
            $product->price = $validatedData['price'];
            $product->description = $validatedData['description'];

            if ($request->hasFile('image')) {
                Storage::delete('public/images/' . $product->image);

                $image = $request->file('image');
                $imageName = time() . '_' . $image->getClientOriginalName();
                $image->storeAs('public/images', $imageName);

                $product->image = $imageName;
            }

            $product->save();

            return redirect()->route('home_page')->with('success', 'Product updated successfully.');
        } catch (\Exception $e) {
            return back()->withErrors(['error' => 'Failed to update product: ' . $e->getMessage()]);
        }
    }


    public function delete($id)
    {
        try {
            $product = ProductsModel::findOrFail($id);

            Storage::delete('public/images/' . $product->image);

            $product->delete();

            return redirect()->route('home_page')->with('success', 'Product deleted successfully.');
        } catch (\Exception $e) {
            return back()->withErrors(['error' => 'Failed to delete product: ' . $e->getMessage()]);
        }
    }
}
