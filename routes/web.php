<?php

use Illuminate\Support\Facades\Route;

Route::get('/login', [\App\Http\Controllers\AuthController::class, 'login_page'])->name('login_page');
Route::get('/register', [\App\Http\Controllers\AuthController::class, 'register_page'])->name("register_page");
Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login'])->name('login');
Route::post('/register', [\App\Http\Controllers\AuthController::class, 'register'])->name("register");
Route::post('/logout', [\App\Http\Controllers\AuthController::class, 'logout'])->name("logout");

Route::middleware(['IsAuthorize'])->group(function () {
    Route::get('/', [\App\Http\Controllers\ProductsController::class, 'home_page'])->name('home_page');
    Route::get('/add', [\App\Http\Controllers\ProductsController::class, 'add_page'])->name('add_page');
    Route::get('/edit/{id}', [\App\Http\Controllers\ProductsController::class, 'edit_page'])->name('edit_page');

    Route::post('/store', [\App\Http\Controllers\ProductsController::class, 'store'])->name('store');
    Route::post('/update/{id}', [\App\Http\Controllers\ProductsController::class, 'update'])->name('update');
    Route::post('/delete/{id}', [\App\Http\Controllers\ProductsController::class, 'delete'])->name('delete');
});
